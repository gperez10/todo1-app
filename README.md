# About 
- todo1-app was developed for TODO1 test
# Implementation
- Auth -> Firebase
- QR generator -> ngx-qrcode2
- QR scan -> cordova-plugin-qrscanner
- API account-> https://mockapi.io/
- API weather->  https://weatherapi.com

# Platform
- ios
- android


