// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    production: true,
    apiKey: 'AIzaSyCBn7xOnzmVKFJ6_5kppG-dXPOCyEtG_pE',
    authDomain: 'todo1-app.firebaseapp.com',
    projectId: 'todo1-app',
    storageBucket: 'todo1-app.appspot.com',
    messagingSenderId: '645620729615',
    appId: '1:645620729615:web:6fad2c62440d44517a607a',
    measurementId: 'G-VW0866LGSG'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
