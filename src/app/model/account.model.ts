export class Account {
  type: string;
  accountNumber: string;
  lastNumber: number;
  amount: number;
  currency: string;
  active: boolean;
  qrAmount: number;
}
