import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from '../model/user.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage {
  [x: string]: any;

  public user: User = new User();

  constructor(private router: Router, public fireauth: AngularFireAuth) {
  }


  async signup() {
    try {
      const response = await this.fireauth.createUserWithEmailAndPassword(
        this.user.email,
        this.user.password
      );
      if (response) {
        this.router.navigate(['/login']);
      }

    } catch (err) {
      alert(err);
    }
  }
}
