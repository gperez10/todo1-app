import { WeatherService } from './../service/weather.service';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/account.service';
import { Account } from '../model/account.model';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { ModalController } from '@ionic/angular';
import { QRPage } from '../modal/qr/qr.page';
import { SentMoneyPage } from '../modal/sent-money/sent-money.page';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  weather: any;
  domElement: any;
  accounts: Account[];
  constructor(
    public weatherService: WeatherService,
    public accountService: AccountService,
    private qrScanner: QRScanner,
    public modalController: ModalController
  ) {

  }
  ngOnInit(): void {
    this.domElement = window.document.querySelector('ion-app') as HTMLElement;
    this.getWeather('bogota');
    this.getAllAccount();
    this.prepare();
  }
  getWeather(city: string) {
    this.weatherService.getWeather(city).subscribe(response => {
      this.weather = response;
    });
  }
  getAllAccount() {
    this.accountService.getAllAccount().subscribe(response => {
      this.accounts = response;
    });
  }
  prepare() {
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        console.log(status.authorized);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  readQR() {
    this.qrScanner.show();
    this.domElement.classList.add('has-camera');

    const scanSub = this.qrScanner.scan()
      .subscribe((text: string) => {
        scanSub.unsubscribe();
        this.onScan(text);
      });
  }

  hideCamera() {
    this.qrScanner.hide();
    this.domElement.classList.remove('has-camera');
  }

  async onScan(data: any) {
    this.hideCamera();
    console.log('data', data);
    const modal = await this.modalController.create({
      component: SentMoneyPage,
      componentProps: {
        data,
      },
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null && dataReturned.data) {
        alert(dataReturned.data);
      }
    });
    return await modal.present();
  }
  ionViewWillLeave() {
    this.hideCamera();
  }

  async generateQR() {
    const modal = await this.modalController.create({
      component: QRPage,
      componentProps: {
        accounts: this.accounts,
      },
    });
    return await modal.present();
  }

}

