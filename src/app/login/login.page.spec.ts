import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPage],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render email and password inputs', () => {
    const compiled = fixture.debugElement.nativeElement;
    const emailInput = compiled.querySelector('ion-input[type="email"]');
    const passwordInput = compiled.querySelector('ion-input[type="password"]');

    expect(emailInput).toBeTruthy();
    expect(passwordInput).toBeTruthy();
  });

  it('should bind email and password inputs to ngModel', () => {
    const emailInput = fixture.debugElement.nativeElement.querySelector('ion-input[type="email"]');
    const passwordInput = fixture.debugElement.nativeElement.querySelector('ion-input[type="password"]');

    // You can test ngModel bindings here
    // For example, set values and check if the component's user object updates
    emailInput.value = 'test@example.com';
    passwordInput.value = 'testpassword';
    emailInput.dispatchEvent(new Event('input'));
    passwordInput.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    expect(component.user.email).toBe('test@example.com');
    expect(component.user.password).toBe('testpassword');
  });

  it('should have "Email" label', () => {
    const compiled = fixture.debugElement.nativeElement;
    const emailLabel = compiled.querySelector('ion-label[for="email"]');

    expect(emailLabel).toBeTruthy();
    expect(emailLabel.textContent).toContain('Email');
  });

  // Add more tests as needed
});
