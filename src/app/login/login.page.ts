import { AngularFireAuth } from '@angular/fire/auth';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../model/user.model';

@Component({
  selector: 'app-home',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {

  public user: User = new User();

  constructor(private router: Router, public fireauth: AngularFireAuth) {
  }


  async login() {
    try {
      const response = await this.fireauth.signInWithEmailAndPassword(
        this.user.email,
        this.user.password
      );
      if (response) {
        this.router.navigate(['/dashboard']);
      }

    } catch (err) {
      alert(err.message);
    }
  }
}
