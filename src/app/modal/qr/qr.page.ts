import { Account } from '../../model/account.model';
import { Component, Input } from '@angular/core';
import { OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})

export class QRPage implements OnInit {
  @Input() accounts: Account[];
  public accountSelected: any;
  public checkAmount: boolean;
  public qrGenerated: boolean;
  public qrData = null;

  constructor(public modalController: ModalController) { }
  ngOnInit(): void {
    this.qrGenerated = false;
    this.checkAmount = false;
  }

  generateQR() {

    this.qrData = JSON.stringify(this.accountSelected);
    this.qrGenerated = true;
  }
  resetAmount() {
    this.accountSelected.qrAmount = 0;
  }


  closeModal() {
    this.modalController.dismiss();
  }

}
