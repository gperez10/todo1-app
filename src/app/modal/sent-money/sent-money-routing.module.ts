import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SentMoneyPage } from './sent-money.page';

const routes: Routes = [
  {
    path: '',
    component: SentMoneyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SentMoneyPageRoutingModule {}
