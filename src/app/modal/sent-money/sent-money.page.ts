import { Component, Input, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Account } from 'src/app/model/account.model';

@Component({
  selector: 'app-sent-money',
  templateUrl: './sent-money.page.html',
  styleUrls: ['./sent-money.page.scss'],
})
export class SentMoneyPage implements OnInit {
  @Input() data: string;
  public amount: number;
  public account: Account;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
    this.account = JSON.parse(this.data);
  }

  closeModal() {
    this.modalController.dismiss();
  }
  sendMoney() {
    this.modalController.dismiss('Successful transaction');
  }

}
