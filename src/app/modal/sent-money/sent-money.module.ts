import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SentMoneyPageRoutingModule } from './sent-money-routing.module';

import { SentMoneyPage } from './sent-money.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SentMoneyPageRoutingModule
  ],
  declarations: [SentMoneyPage]
})
export class SentMoneyPageModule {}
